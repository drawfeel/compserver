一个简单的python/django web服务器，用来为三方用户提供lib bin apk等编译与编译产物下载。实现项目SDK对三方用户的隔离，同时方便三方用户编译调试。
用户需要将需要编译的模块，放在若干个文件夹中，同时文件夹中有一个Android.mk。这些文件夹全部打包成一个zip包。zip包的名字可以任意，但是zip里面就是各个文件夹。

### 1，修改django配置

修改SDK所在目录位置，以及编译后拷贝编译产物的位置
compserver/controller.py

### 2，修改编译sh脚本

根据实际需要修改如下两个文件：
makeo.sh
mmm.sh

### 3，启动服务

在shell里面运行如下两个命令启动服务：
 ./go.sh
sudo ./nerccmd_server/restart.sh

备注：为了不让django有root权限，而编译脚本有root权限，所以要分别启动上述两个服务。这两个服务用socket通信。
