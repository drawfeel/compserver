while true
do
	ps -ef | grep "nerccmd" | grep -v "grep"
	if [ "$?" -eq 1 ]
	then
		./nerccmd
		echo "process has been restarted!"
	else
		echo "process already started!"
	fi
	sleep 30
done
